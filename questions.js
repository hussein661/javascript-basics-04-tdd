
const stringSize = (text) => {
   return text.length;
  }
const replaceCharacterE = (text) => {
    var newstr = text.replace('e',' ');
    return newstr;
}
const concatString = (text1, text2) => {
    return text1 + text2;

}
const showChar5 = (text) => {
    return text.charAt(4);
}
const showChar9 = (text) => {
    return text.substring(0,9);
}
const toCapitals = (text) => {
    return text.toUpperCase();
}
const toLowerCase = (text) => {
    return text.toLowerCase();

}
const removeSpaces = (text) => {
 return text.trim();

}
    
const IsString = (text) => {
if (typeof(text) == "string"){
    return true;}
    else {
        return false;
    }
}

const getExtension = (text) => {
    return text.split('.').pop();
}
const countSpaces = (text) => {
return text.split(" ").length - 1;
}
const InverseString = (text) => {
    var newstr = "";
for (var i=text.length -1;i >= 0;i--){
newstr += text.charAt(i);
}
return newstr;
}

const power = (x, y) => {
return Math.pow(x,y);
}
const absoluteValue = (num) => {
return Math.abs(num);
}
const absoluteValueArray = (array) => {
varNew =  array.map(Math.abs);
return varNew;
}
const circleSurface = (radius) => {
return Math.round(Math.PI * radius * radius);
}
const hypothenuse = (ab, ac) => {
 return Math.sqrt(ab * ab + ac * ac);
}
const BMI = (weight, height) => {
return parseFloat((weight/(height*height)).toFixed(2));
}

const createLanguagesArray = () => {
var languages = ["Html","CSS","Java","PHP"];
return languages;
}

const createNumbersArray = () => {
return [0,1,2,3,4,5];

}

const replaceElement = (languages) => {
languages[2] = "Javascript";
return languages;
}

const addElement = (languages) => {
languages.push("Ruby","Python");
return languages;
}

const addNumberElement = (numbers) => {
var numbers = createNumbersArray();
numbers.unshift(-2,-1);
return numbers;
}

const removeFirst = (languages) => {
languages.shift();
return languages;
}

const removeLast = (languages) => {
languages.pop();
return languages;
}

const convertStrToArr = (social_arr) => {
    var arr = social_arr.split(",");
    return arr;
}

const convertArrToStr = (languages) => {
return languages.toString();
}

const sortArr = (social_arr) => {
return social_arr.sort();
}

const invertArr = (social_arr) => {
return social_arr.reverse();
}
